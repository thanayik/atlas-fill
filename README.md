# atlas_fill

`atlas_fill` is a python module designed to create images filled with values according to the grey matter atlas listed here: https://git.fmrib.ox.ac.uk/falmagro/UK_biobank_pipeline_v_1/-/tree/master/templates/GMatlas.

## usage

If you want to download and use a zip archive of this code then use this link

https://git.fmrib.ox.ac.uk/thanayik/atlas-fill/-/archive/master/atlas-fill-master.zip

else, use git clone

`git clone git@git.fmrib.ox.ac.uk:thanayik/atlas-fill.git`

`cd atlas_fill`

```
usage: atlas_fill.py [-h] --csv-file CSV_FILE --out-path OUT_PATH
                     --atlas-image ATLAS_IMAGE --atlas-labels ATLAS_LABELS

fill atlas regions with custom values

required arguments:
  -h, --help            show this help message and exit
  --csv-file CSV_FILE   csv file with 2 columns. First column is text strings
                        for labels, second column is region integer index
  --out-path OUT_PATH   the full path of the output nifti file to save with
                        new atlas values
  --atlas-image ATLAS_IMAGE
                        path to atlas nifti file
  --atlas-labels ATLAS_LABELS
                        path to atlas labels txt file

Example: fslpython atlas_fill/atlas_fill.py --csv-file my_csv_data.csv --out-path /path/to/result/image.nii.gz --atlas-labels atlas_fill/GMatlas.txt --atlas-image atlas_fill/GMatlas.nii.gz
```

## testing

```
python -m venv venv
source venv/bin/activate
pip install -U pytest fslpy
pytest -srAv atlas_fill/atlas_fill.py # expect some failures if there are region mismatches (your csv omits some regions in the GMatlas.txt file)
```
