#!/usr/bin/env fslpython
'''
fill biobank grey matter atlas with values from a csv file.

grey matter atlas is from: https://git.fmrib.ox.ac.uk/falmagro/UK_biobank_pipeline_v_1/-/tree/master/templates/GMatlas

to test using pytest: pytest -srAv atlas_fill/atlas_fill.py

'''

import sys
import csv
import nibabel as nii
import argparse

parser = argparse.ArgumentParser(
  description='fill atlas regions with custom values',
  epilog='Example: fslpython atlas_fill.py --csv-file my_csv_data.csv --out-path /path/to/result/image.nii.gz --atlas-labels GMatlas.txt --atlas-image GMatlas.nii.gz'
)
parser.add_argument('--csv-file', type=str, help='csv file with 2 columns. First column is text strings for labels, second column is region integer index', required=True)
parser.add_argument('--out-path', type=str, help='the full path of the output nifti file to save with new atlas values', required=True)
parser.add_argument('--atlas-image', type=str, help='path to atlas nifti file', required=True)
parser.add_argument('--atlas-labels', type=str, help='path to atlas labels txt file', required=True)

def load_csv(csv_path, remove_header=True):
    '''
    load the csv row and columns and return a list
    '''
    with open(csv_path) as csvfile:
        csvreader = csv.reader(csvfile, quotechar='"', delimiter=',', quoting=csv.QUOTE_ALL)
        contents = list(csvreader)
    if remove_header:
        contents = contents[1:]
    return contents

def test_load_csv_omit_header():
    '''
    test loading csv file and remove header
    '''
    import os
    csv_path = os.path.join(os.path.dirname(__file__), "test_data.csv")
    contents = load_csv(csv_path)
    assert len(contents) == 151

def test_load_csv_with_header():
    '''
    test loading csv file with header
    '''
    import os
    csv_path = os.path.join(os.path.dirname(__file__), "test_data.csv")
    contents = load_csv(csv_path, remove_header=False)
    assert len(contents) == 152

def load_atlas_labels(txt_path):
    '''
    load txt file lines to list
    '''
    with open(txt_path, 'r') as txtfile:
        contents = [line.rstrip() for line in txtfile]
    return contents

def test_load_atlas_labels():
    '''
    test loading of atlas labels and verify number of rows loaded
    '''
    import os
    atlas_label_path = os.path.join(os.path.dirname(__file__), "GMatlas.txt")
    contents = load_atlas_labels(atlas_label_path)
    assert len(contents) == 139



def atlas_fill(csv_file, out_path, atlas_image, atlas_labels):
    '''
    do main processing. make a new atlas nifti file filled with values from the csv file.
    '''
    fill_data = load_csv(csv_file)
    label_data = load_atlas_labels(atlas_labels)

    aimg = nii.load(atlas_image) # load the nifti atlas image
    A = aimg.get_fdata() # get the numpy array
    A_out = A.copy() # copy the image data to a new array
    A_out.fill(0) # fill the copy with zeros
    out_regions = 0
    regions_used = []
    regions_avail = []
    filled_regions = []
    for fill_row in fill_data:
        fill_region = fill_row[0].lower().replace("volume of grey matter in ", "")
        # "volume of " is used in some subcortical regions. Most other regions use  "volume of grey matter "
        #fill_region = fill_region.replace("volume of ","")
        fill_value = float((fill_row[1]))
        if "(left)" in fill_region:
            fill_region = fill_region.replace("(left)","")
            fill_region = "left " + fill_region
        elif "(right)" in fill_region:
            fill_region = fill_region.replace("(right)","")
            fill_region = "right " + fill_region
        elif "(vermis)" in fill_region:
            fill_region = fill_region.replace("(vermis)","")
            fill_region = "vermis " + fill_region
        regions_avail.append(fill_region)

        for atlas_idx, atlas_region in enumerate(label_data):
            atlas_string = atlas_region.lower()
            if atlas_string in fill_region:
                vfill = 0
                if fill_value < 0:
                    vfill = -1 - fill_value
                elif fill_value > 0:
                    vfill = 1 - fill_value
                A_out[A == int(atlas_idx)+1] = float(vfill)
                filled_regions.append((int(atlas_idx)+1, float(vfill)))
                out_regions = out_regions + 1
                regions_used.append(fill_region)
    # used for debugging
    #diff = set(regions_avail) - set(regions_used)
    #[print(val) for val in diff]
    #n_regions = len(regions_avail)
    #print(n_regions)
    #print(out_regions)
    out_img = nii.Nifti1Image(A_out, aimg.affine, aimg.header) # make a new nifti image from A_out data. Has same header info as A
    nii.save(out_img, out_path) # write the output file so it is saved.
    return filled_regions

def test_atlas_fill():
    '''
    test atlas fill values match values from csv file (min and max) rounded to 2 decimal places
    '''
    import os
    import tempfile
    import numpy
    atlas_labels = os.path.join(os.path.dirname(__file__), "GMatlas.txt")
    atlas_image = os.path.join(os.path.dirname(__file__), "GMatlas.nii.gz")
    csv_file = os.path.join(os.path.dirname(__file__), "test_data.csv")

    test_dir = tempfile.mkdtemp()
    out_path = os.path.join(test_dir, 'test_result.nii.gz')
    filled_regions = atlas_fill(csv_file=csv_file, out_path=out_path, atlas_image=atlas_image, atlas_labels=atlas_labels)

    vals = [region[1] for region in filled_regions]
    csv_idxs = [region[0] for region in filled_regions]
    csv_idxs.sort()
    min_val = round(min(vals), 2)
    max_val = round(max(vals), 2)
    img = nii.load(out_path)
    I = img.get_fdata()
    min_img = round(I.min(), 2)
    max_img = round(I.max(), 2)

    atlas_img = nii.load(atlas_image)
    A = atlas_img.get_fdata().astype(int)+1
    atlas_idxs = numpy.unique(A)
    errors = []
    #if there are regions missing/unsupported in the csv data then this test will fail
    if min_val != min_img:
        errors.append('csv data min and img data min do not match: {}..{}'.format(min_val, min_img))
    if max_val != max_img:
        errors.append('csv data max and img data max do not match: {}..{}'.format(max_val, max_img))
    if list(csv_idxs) != list(atlas_idxs):
        errors.append('csv regions do not match range of regions in atlas')
    #assert (min_val == min_img) and (max_val == max_img) and ()
    assert not errors, "errors occured:\n{}".format("\n".join(errors))

if __name__ == "__main__":
  args = parser.parse_args()
  csv_file = args.csv_file
  out_path = args.out_path
  atlas_image = args.atlas_image
  atlas_labels = args.atlas_labels
  sys.exit(atlas_fill(csv_file=csv_file, out_path=out_path, atlas_image=atlas_image, atlas_labels=atlas_labels))
